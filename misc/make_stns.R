library(dplyr)
library(readr)
library(stringr)
library(tibble)
library(XML)

f <- tempfile()
url <- "http://sheridan.geog.kent.edu/ssc/stations.html"
download.file(url, f)

tabs <- readHTMLTable(f)
tabs <- tabs[[1]]
stns <- read_csv("data/stations.csv")

# anti_join(tabs, stns, by = c("ID" = "code")) # 2 stations not in 
# anti_join(stns, tabs, by = c("code" = "ID")) # 1 station not in date list

stns <- left_join(tabs, stns, by = c("ID" = "code"))
stns <- filter(stns, grepl("Canada", Station))
stns$Station <- sub("\\s*, Canada", "", stns$Station)

years <- str_extract_all(stns$Years, "[0-9]{4}")
start <- sapply(years, function (x) min(as.integer(x)))
end <- sapply(years, function (x) max(as.integer(x)))

stns <- add_column(stns, start = start, end = end, .after = 3)

# stns <- add_column(stns, Province = sub("^\\s*", "", sapply(strsplit(stns$Station, ","), `[[`, 2)), .after = 1)
# stns$Station <- sapply(strsplit(stns$Station, ","), `[[`, 1)

saveRDS(stns, "data/stations.rds")
